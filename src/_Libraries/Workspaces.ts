import {
	apply, applyTemplates, chain, empty, mergeWith, move, schematic, url,
	Rule, SchematicsException
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { latestVersions } from '@schematics/angular/utility/latest-versions';

import { InstallPackages } from './Packages';

import { Schema as WorkspaceOptions } from '../Workspace/Schema';


const FILES = '../Workspace/files';

const RequiredOptionsAndDefaults = (options: WorkspaceOptions) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}

	if (!options.directory) {
		options.directory = strings.classify(options.name);
	}

	if (!options.newProjectRoot) {
		options.newProjectRoot = 'Projects';
	}

	if (!options.version) {
		// This will only work sometimes...
		options.version = latestVersions.Angular;
	}
};

// TODO: What if you want to create a workspace without Ng-New? Then you need the two commented out lines.
// TODO: But if you add them and call Ng-New, it breaks the Application schematic.
export const WorkspaceRule = (options: WorkspaceOptions): Rule => {
	RequiredOptionsAndDefaults(options);

	return chain([
		mergeWith(apply(url(FILES), [
			applyTemplates({ utils: strings, ...options, dot: '.', latestVersions }),
			//move(options.directory as string)
		])),
		//InstallPackages(options.directory as string)
	]);
};
