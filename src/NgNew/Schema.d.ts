export interface Schema {
	workspace: string;
	app: string;
	newProjectRoot?: string;
	directory?: string;
	version: string;
	viewEncapsulation?: ViewEncapsulation;
	skipInstall?: boolean;
}

export enum ViewEncapsulation {
	Emulated = "Emulated",
	Native = "Native",
	None = "None",
	ShadowDom = "ShadowDom",
}

