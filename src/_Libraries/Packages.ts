import { addPackageJsonDependency, NodeDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { SchematicContext, Tree } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';

export const BootstrapDependencies: NodeDependency[] = [
	{
		type: NodeDependencyType.Default,
		name: "bootstrap",
		version: "^4.0.0"
	}, {
		type: NodeDependencyType.Default,
		name: "@ng-bootstrap/ng-bootstrap",
		version: "^5.0.0"
	}
];

const FontAwesomeVersion = "^5.0.0";
export const FontAwesomeDependencies: NodeDependency[] = [
	{
		type: NodeDependencyType.Default,
		name: "@fortawesome/fontawesome-svg-core",
		version: "^1.0.0"
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/free-solid-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/free-regular-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/free-brands-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/angular-fontawesome",
		version: "^0.5.0"
	},
]; // 0.5.0 = Angular 8, 0.6.0 = Angular 9

export const FontAwesomeProDependencies: NodeDependency[] = [
	{
		type: NodeDependencyType.Default,
		name: "@fortawesome/fontawesome-pro",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/pro-duotone-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/pro-light-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/pro-regular-svg-icons",
		version: FontAwesomeVersion
	}, {
		type: NodeDependencyType.Default,
		name: "@fortawesome/pro-solid-svg-icons",
		version: FontAwesomeVersion
	},
];


/**** Package Functions ****/

export const InstallPackages = (rootDir: string) =>
	(host: Tree, context: SchematicContext): Tree => {
		context.addTask(new NodePackageInstallTask(rootDir));
		return host;
	};

export const AddPackagesToPackageJson = (packages: NodeDependency[]) =>
	(host: Tree, context: SchematicContext): Tree => {
		packages.forEach((dependency) => addPackageJsonDependency(host, dependency));
		return host;
	};

