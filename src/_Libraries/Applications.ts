import {
	apply, applyTemplates, chain, mergeWith, move, url, noop, schematic,
	MergeStrategy, Rule, SchematicContext, SchematicsException, Tree
} from '@angular-devkit/schematics';
import { join, normalize, strings } from '@angular-devkit/core';
import { validateProjectName } from '@schematics/angular/utility/validation';
import { getWorkspace } from '@schematics/angular/utility/workspace';
import { relativePathToWorkspaceRoot } from '@schematics/angular/utility/paths';
import { updateWorkspace } from '@schematics/angular/utility/workspace';

import { AddPackagesToPackageJson, BootstrapDependencies, FontAwesomeDependencies, InstallPackages } from './Packages';
import { ProjectDefinition } from './Project';

import { Schema as ApplicationOptions } from '../Application/Schema';
import { GetProjectRoot } from './Common';
import { addSymbolToNgModuleMetadata } from '@schematics/angular/utility/ast-utils';
import { ReadIntoSourceFile } from './AST';
import { InsertChange } from '@schematics/angular/utility/change';


const FILES = '../Application/files';


const RequiredOptionsAndDefaults = async (options: ApplicationOptions, host: Tree) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}
	options.name = strings.classify(options.name);

	validateProjectName(options.name);

	if (!options.projectRoot) {
		options.projectRoot = await GetProjectRoot(options.name, host);
	}

	if (!options.skipInstall) {
		options.skipInstall = false;
	}
};

const AddProjectToWorkspace = (options: ApplicationOptions): Rule => {
	const project = ProjectDefinition(options);

	return updateWorkspace((workspace) => {
		if (workspace.projects.size === 0) {
			workspace.extensions.defaultProject = options.name;
		}

		workspace.projects.add({ name: options.name, ...project});
	});
};

const AddModuleToApplication = (appModule: string, modulePath: string, moduleName: string): Rule =>
	(host: Tree, context: SchematicContext) => {
		const sourceFile = ReadIntoSourceFile(host, appModule);
		const changes = addSymbolToNgModuleMetadata(sourceFile, modulePath, 'imports', moduleName, modulePath);

		const changeRecorder = host.beginUpdate(appModule);
		for (let change of changes) {
			if (change instanceof InsertChange) {
				changeRecorder.insertLeft(change.pos, change.toAdd);
			}
		}
		host.commitUpdate(changeRecorder);

		return host;
	};

const AddPackagesToSharedModule = (modulePath: string): Rule =>
	(host: Tree, context: SchematicContext) => {
		const module = `${modulePath}/Shared.Module.ts`;
		const sourceFile = ReadIntoSourceFile(host, module);

		let changes = addSymbolToNgModuleMetadata(sourceFile, modulePath, 'imports', 'NgbModule', '@ng-bootstrap/ng-bootstrap');
		changes = changes.concat(addSymbolToNgModuleMetadata(sourceFile, modulePath, 'imports', 'FontAwesomeModule', '@fortawesome/angular-fontawesome'));

		const changeRecorder = host.beginUpdate(module);
		for (let change of changes) {
			if (change instanceof InsertChange) {
				changeRecorder.insertLeft(change.pos, change.toAdd);
			}
		}
		host.commitUpdate(changeRecorder);

		return host;
	};

export const ApplicationRule = (options: ApplicationOptions): Rule =>
	async (host: Tree, context: SchematicContext) => {
		await RequiredOptionsAndDefaults(options, host);

		const appRoot = `${options.projectRoot as string}/${options.name}`;
		const appModule = `${appRoot}/src/Application/Application.Module.ts`;
		const modulePath = `${appRoot}/src/Modules`;

		return chain([
			AddProjectToWorkspace(options),
			mergeWith(apply(url(FILES), [
				applyTemplates({
					utils: strings,
					...options,
					relativePathToWorkspaceRoot: relativePathToWorkspaceRoot(appRoot),
					appName: options.name
				}),
				move(appRoot)
			]), MergeStrategy.Overwrite),
			schematic('module', { name: 'Pages', project: options.name }),
			schematic('component', { name: 'HelloWorld' , project: options.name, module: 'Pages' }),
			schematic('module', { name: 'Shared', project: options.name, includeRouting: false }),
			AddModuleToApplication(appModule, `@Modules/Pages`, 'PagesModule'),
			AddModuleToApplication(appModule, `@Modules/Shared`, 'SharedModule'),
			AddPackagesToSharedModule(`${modulePath}/Shared`),
			AddPackagesToPackageJson([
				...BootstrapDependencies,
				...FontAwesomeDependencies
			]),
			options.skipInstall ? noop() : InstallPackages('/')
		]);
	};
