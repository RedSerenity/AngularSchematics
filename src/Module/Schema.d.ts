export interface Schema {
	name: string;
	path?: string;
	project?: string;
	includeRouting?: boolean;
}
