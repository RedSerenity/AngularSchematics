import {
	apply, chain, mergeWith, url, applyTemplates, move,
	Rule, SchematicContext, SchematicsException, Tree
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { parseName } from '@schematics/angular/utility/parse-name';
import { validateHtmlSelector, validateName } from '@schematics/angular/utility/validation';

import { Schema as DirectiveOptions } from '../Directive/Schema';
import { MODULE_DIR } from './Modules';
import { AddExportToBarrel, dirExists, GetDefaultProject, GetProjectRoot } from './Common';
import { AddToModuleArray } from './AST';


const FILES = '../Directive/files';


const RequiredOptionsAndDefaults = async (options: DirectiveOptions, host: Tree) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}
	options.name = strings.classify(options.name);

	if (!options.project) {
		options.project = await GetDefaultProject(host);
	}

	const projectRoot = await GetProjectRoot(options.project, host);
	const modulePath = `${projectRoot}/${MODULE_DIR}`;

	if (!options.path) {
		if (!dirExists(`${modulePath}/${options.module}`, host)) {
			const classifiedModule = strings.classify(options.module);

			if (dirExists(`${modulePath}/${classifiedModule}`, host)) {
				options.path = `${modulePath}/${classifiedModule}/Directives`;
			} else {
				throw new SchematicsException(`Could not find module ${options.module} in project ${options.project}. Please check the name and format.`);
			}
		} else {
			options.path = `${modulePath}/${options.module}/Directives`;
		}
	}

	const parsedPath = parseName(options.path, options.name);
	options.name = parsedPath.name;
	options.path = parsedPath.path;

	if (!options.selector) {
		options.selector = strings.dasherize(options.name);
	}

	validateName(options.name);
	validateHtmlSelector(options.selector as string);

	return modulePath;
};

export const DirectiveRule = (options: DirectiveOptions): Rule =>
	async (host: Tree, context: SchematicContext) => {
		const modulePath = await RequiredOptionsAndDefaults(options, host);

		return chain([
			AddToModuleArray({
				modulePath: modulePath,
				moduleName: options.module,
				itemType: 'Directive',
				itemName: options.name
			}),
			AddExportToBarrel(options.path as string, `${options.name}.Directive`),
			mergeWith(apply(url(FILES), [
				applyTemplates({ ...strings, ...options }),
				move(options.path as string)
			]))
		]);
	};
