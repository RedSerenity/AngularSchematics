export interface Schema {
	name: string;
	module: string;
	project?: string;
	path?: string;
	selector?: string;
}
