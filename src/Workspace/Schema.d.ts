export interface Schema {
	name: string;
	newProjectRoot?: string;
	directory?: string;
	version: string;
}
