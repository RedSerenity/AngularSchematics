export interface Schema {
	name: string;
	module: string;
	project?: string;
	path?: string;
	viewEncapsulation?: ViewEncapsulation;
	changeDetection?: ChangeDetection;
	selector?: string;
}

export enum ViewEncapsulation {
	Emulated = "Emulated",
	Native = "Native",
	None = "None",
	ShadowDom = "ShadowDom"
}

export enum ChangeDetection {
	Default = "Default",
	OnPush = "OnPush"
}
