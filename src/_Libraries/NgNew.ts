import {
	apply, chain, empty, mergeWith, move, schematic,
	Rule,	SchematicContext,	SchematicsException, Tree
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { RepositoryInitializerTask } from '@angular-devkit/schematics/tasks';

import { AddPackagesToPackageJson, InstallPackages } from './Packages';

import { Schema as NgNewOptions } from '../NgNew/Schema';
import { Schema as WorkspaceOptions } from '../Workspace/Schema';
import { Schema as ApplicationOptions } from '../Application/Schema';
import { latestVersions } from '@schematics/angular/utility/latest-versions';


const RequiredOptionsAndDefaults = (options: NgNewOptions) => {
	if (!options.workspace) {
		throw new SchematicsException('Invalid Options: "workspace" is required.');
	}

	if (!options.app) {
		throw new SchematicsException('Invalid Options: "app" is required.');
	}

	if (!options.directory) {
		options.directory = strings.classify(options.workspace);
	} else {
		options.directory = strings.classify(options.directory);
	}

	if (!options.version) {
		// This will only work sometimes...
		options.version = latestVersions.Angular;
	}
};

const SetWorkspaceOptions = (options: NgNewOptions): WorkspaceOptions => <WorkspaceOptions> {
	name: strings.classify(options.workspace),
	newProjectRoot: options.newProjectRoot || 'Projects',
	directory: options.directory,
	version: options.version
};

const SetApplicationOptions = (options: NgNewOptions): ApplicationOptions => <ApplicationOptions> {
	name: strings.classify(options.app),
	projectRoot: options.newProjectRoot || 'Projects',
	viewEncapsulation: options.viewEncapsulation,
	skipInstall: options.skipInstall
};

const InitializeRepository = (options: NgNewOptions) =>
	(host: Tree, context: SchematicContext) => {
		context.addTask(new RepositoryInitializerTask(options.directory, {}));
	};

export const NgNewRule = (options: NgNewOptions): Rule => {
	RequiredOptionsAndDefaults(options);

	const workspaceOptions: WorkspaceOptions = SetWorkspaceOptions(options);
	const applicationOptions: ApplicationOptions = SetApplicationOptions(options);

	return chain([
		mergeWith(apply(empty(), [
			schematic('workspace', workspaceOptions),
			schematic('application', applicationOptions),
			move(options.directory as string)
		])),
		InstallPackages(options.directory as string),
		InitializeRepository(options)
	]);
};
