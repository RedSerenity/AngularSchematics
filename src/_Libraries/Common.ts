import { Rule, Tree } from '@angular-devkit/schematics';
import { getWorkspace } from '@schematics/angular/utility/workspace';
import { join, normalize } from '@angular-devkit/core';

export const GetProjectRoot = async (projectName: string, host: Tree) => {
	const workspace = await getWorkspace(host);
	const newProjectRoot = workspace.extensions.newProjectRoot as (string | undefined) || 'Projects';
	return join(normalize(newProjectRoot), projectName);
};

export const GetDefaultProject = async (host: Tree) => {
	const workspace = await getWorkspace(host);
	return workspace.extensions.defaultProject as string;
};

export const UpdateBarrelFile = (barrelPath: string, newContent: string | Buffer): Rule =>
	(tree: Tree) => {
		const barrelFile = barrelPath + '/index.ts';

		if (!tree.exists(barrelFile)) {
			tree.create(barrelFile, newContent);
		} else {
			const existingContent = tree.read(barrelFile);

			if (existingContent) {
				tree.overwrite(barrelFile, existingContent.toString() + newContent);
			}
		}
	};

export const AddExportToBarrel = (barrelPath: string, itemPath: string): Rule =>
	UpdateBarrelFile(barrelPath, `export * from './${itemPath}'\n`);

export const dirExists = (dirPath: string, host: Tree) => !host.exists(dirPath) && host.getDir(dirPath).subfiles.length > 0;
