import { join, normalize } from '@angular-devkit/core';
import { Builders, ProjectType } from '@schematics/angular/utility/workspace-models';

import { Schema as ApplicationOptions } from '../Application/Schema';

export const ProjectDefinition = (options: ApplicationOptions) => {
	const projectRoot = `${options.projectRoot as string}/${options.name}`;
	const sourceRoot = join(normalize(projectRoot), 'src');

	return {
		root: normalize(projectRoot),
		sourceRoot,
		projectType: ProjectType.Application,
		prefix: 'application',
		cli: {
			defaultCollection: "@RedSerenity/Schematics"
		},
		schematics: {},
		targets: {
			build: {
				builder: Builders.Browser,
				options: {
					outputPath: `Dist/${options.name}`,
					index: `${sourceRoot}/index.html`,
					main: `${sourceRoot}/Main.ts`,
					polyfills: `${sourceRoot}/Polyfills.ts`,
					tsConfig: `${projectRoot}/tsconfig.app.json`,
					aot: true,
					assets: [
						`${sourceRoot}/favicon.ico`,
						`${sourceRoot}/Assets`,
					],
					styles: [
						`${sourceRoot}/Styles/Styles.scss`,
					],
					scripts: [],
				},
				configurations: {
					production: {
						fileReplacements: [{
							replace: `${sourceRoot}/Config/Config.ts`,
							with: `${sourceRoot}/Config/Config.Production.ts`,
						}],
						optimization: true,
						outputHashing: 'all',
						sourceMap: false,
						extractCss: true,
						namedChunks: false,
						aot: true,
						extractLicenses: true,
						vendorChunk: false,
						buildOptimizer: true,
						budgets: [
							{
								type: 'initial',
								maximumWarning: '2mb',
								maximumError: '5mb',
							},
							{
								type: 'anyComponentStyle',
								maximumWarning: '6kb',
								maximumError: '10kb',
							}],
					},
				},
			},
			serve: {
				builder: Builders.DevServer,
				options: {
					browserTarget: `${options.name}:build`,
				},
				configurations: {
					production: {
						browserTarget: `${options.name}:build:production`,
					},
				},
			},
			'extract-i18n': {
				builder: Builders.ExtractI18n,
				options: {
					browserTarget: `${options.name}:build`,
				},
			},
			test : {
				builder: Builders.Karma,
				options: {
					main: `${sourceRoot}/Test.ts`,
					polyfills: `${sourceRoot}/Polyfills.ts`,
					tsConfig: `${projectRoot}/tsconfig.spec.json`,
					karmaConfig: `${projectRoot}/karma.conf.js`,
					assets: [
						`${sourceRoot}/favicon.ico`,
						`${sourceRoot}/Assets`,
					],
					styles: [
						`${sourceRoot}/Styles/Styles.scss`,
					],
					scripts: [],
				},
			},
			lint: {
				builder: Builders.TsLint,
				options: {
					tsConfig: [
						`${projectRoot}/tsconfig.app.json`,
						`${projectRoot}/tsconfig.spec.json`,
					],
					exclude: [
						'**/node_modules/**',
					],
				},
			},
		},
	};
};
