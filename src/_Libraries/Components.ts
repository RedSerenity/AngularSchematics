import {
	apply, applyTemplates, chain, mergeWith, move, url,
	Rule, SchematicContext, SchematicsException, Tree
} from '@angular-devkit/schematics';
import { normalize, strings } from '@angular-devkit/core';
import { parseName } from '@schematics/angular/utility/parse-name';
import { validateHtmlSelector, validateName } from '@schematics/angular/utility/validation';

import { Schema as ComponentOptions } from '../Component/Schema';
import { MODULE_DIR } from './Modules';
import { AddToModuleArray } from './AST';
import { AddExportToBarrel, dirExists, GetDefaultProject, GetProjectRoot } from './Common';


const FILES = '../Component/files';


const RequiredOptionsAndDefaults = async (options: ComponentOptions, host: Tree) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}
	options.name = strings.classify(options.name);

	if (!options.project) {
		options.project = await GetDefaultProject(host);
	}

	const projectRoot = await GetProjectRoot(options.project, host);
	const moduleBasePath = `${projectRoot}/${MODULE_DIR}`;

	if (!options.path) {
		if (!dirExists(`${moduleBasePath}/${options.module}`, host)) {
			const classifiedModule = strings.classify(options.module);

			if (dirExists(`${moduleBasePath}/${classifiedModule}`, host)) {
				options.path = `${moduleBasePath}/${classifiedModule}/Components`;
			} else {
				throw new SchematicsException(`Could not find module ${options.module} in project ${options.project}. Please check the name and format.`);
			}
		} else {
			options.path = `${moduleBasePath}/${options.module}/Components`;
		}
	}

	const parsedPath = parseName(options.path, options.name);
	options.name = parsedPath.name;
	options.path = parsedPath.path;

	validateName(options.name);
	validateHtmlSelector(options.selector as string);

	return moduleBasePath;
};

export const ComponentRule = (options: ComponentOptions): Rule =>
	async (host: Tree, context: SchematicContext) => {
		const modulePath = await RequiredOptionsAndDefaults(options, host);

		return chain([
			AddToModuleArray({
				modulePath: modulePath,
				moduleName: options.module,
				itemType: 'Component',
				itemName: options.name
			}),
			AddExportToBarrel(options.path as string, `${options.name}/${options.name}.Component`),
			mergeWith(apply(url(FILES), [
				applyTemplates({ ...strings, ...options }),
				move(options.path as string)
			]))
		]);
	};
