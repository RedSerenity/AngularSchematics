export interface Schema {
	name: string;
	projectRoot?: string;
	viewEncapsulation?: ViewEncapsulation;
	skipInstall?: boolean;
}

export enum ViewEncapsulation {
	Emulated = "Emulated",
	Native = "Native",
	None = "None",
	ShadowDom = "ShadowDom",
}
