import {
	apply, applyTemplates, chain, mergeWith, move, url, filter, noop,
	Rule, SchematicContext, SchematicsException, Tree, MergeStrategy
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { parseName } from '@schematics/angular/utility/parse-name';

import { Schema as ModuleOptions } from '../Module/Schema';
import { GetDefaultProject, GetProjectRoot } from './Common';


export const MODULE_DIR='/src/Modules';
const FILES='../Module/files';


const RequiredOptionsAndDefaults = async (options: ModuleOptions, host: Tree) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}
	options.name = strings.classify(options.name);

	if (!options.project) {
		options.project = await GetDefaultProject(host);
	}

	if (!options.path) {
		const projectRoot = await GetProjectRoot(options.project, host);
		options.path = `${projectRoot}/${MODULE_DIR}`;
	}

	const parsedPath = parseName(options.path, options.name);
	options.name = parsedPath.name;
	options.path = parsedPath.path;

	if (options.includeRouting === undefined) {
		options.includeRouting = true;
	}
};

const pagesSkip = [
	'/__name@classify__/Directives/index.ts.template',
	'/__name@classify__/Models/index.ts.template',
	'/__name@classify__/Pipes/index.ts.template',
	'/__name@classify__/Services/index.ts.template'
];

export const ModuleRule = (options: ModuleOptions): Rule =>
	async (host: Tree, context: SchematicContext) => {
		await RequiredOptionsAndDefaults(options, host);

		return chain([
			mergeWith(apply(url(FILES), [
				options.includeRouting ? noop() : filter(path =>
					!path.endsWith('.Routes.ts.template') &&
					!path.endsWith('.RoutingModule.ts.template')
				),
				options.name !== 'Pages' ? noop() : filter(path => !pagesSkip.includes(path)),
				applyTemplates({ ...strings, ...options, isPages: options.name === 'Pages' }),
				move(options.path as string)
			]), MergeStrategy.Overwrite)
		]);
	};
