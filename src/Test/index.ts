import { chain, empty, mergeWith, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { AddToModuleArray } from '../_Libraries/AST';


export default (): Rule =>
	(host: Tree, context: SchematicContext) => {

	return chain([
		AddToModuleArray({
			modulePath: '/Projects/B1/src/Modules/',
			moduleName: 'NewMod',
			itemType: 'Component',
			itemName: 'NewComp'
		})
	]);
};
