import {
	apply, chain, mergeWith, url, applyTemplates, move,
	Rule, SchematicContext, SchematicsException, Tree, schematic
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { parseName } from '@schematics/angular/utility/parse-name';
import { validateHtmlSelector, validateName } from '@schematics/angular/utility/validation';
import * as fs from "fs";

import { Schema as PageOptions } from '../Page/Schema';
import { Schema as ComponentOptions } from '../Component/Schema';
import { AddExportToBarrel, GetDefaultProject, GetProjectRoot } from './Common';
import { AddToModuleArray } from './AST';


const PAGES_DIR = 'Pages';
const FILES = '../Page/files';


const RequiredOptionsAndDefaults = async (options: PageOptions, host: Tree) => {
	if (!options.name) {
		throw new SchematicsException('Invalid Options: "name" is required.');
	}
	options.name = strings.classify(`Page${options.name}`);

	if (!options.project) {
		options.project = await GetDefaultProject(host);
	}

	const projectRoot = await GetProjectRoot(options.project, host);
	const pagePath = `${projectRoot}/${PAGES_DIR}`;

	if (!options.path) {
		options.path = `${pagePath}/${options.name}`;
	}

	const parsedPath = parseName(options.path, options.name);
	options.name = parsedPath.name;
	options.path = parsedPath.path;

	if (!options.selector) {
		options.selector = strings.dasherize(options.name);
	}

	validateName(options.name);
	validateHtmlSelector(options.selector as string);
};

export const PageRule = (options: PageOptions): Rule =>
	async (host: Tree, context: SchematicContext) => {
		await RequiredOptionsAndDefaults(options, host);

		const componentOptions: ComponentOptions = {
			module: 'Pages',
			name: options.name,
			selector: options.selector
		};

		return chain([
			schematic('component', componentOptions),
			/*mergeWith(apply(url(FILES), [
				applyTemplates({ ...strings, ...options }),
				move(options.path as string)
			]))*/
		]);
	};
