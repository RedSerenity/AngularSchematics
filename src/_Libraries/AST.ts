import { SchematicContext, SchematicsException, Tree } from '@angular-devkit/schematics';
import { insertImport } from '@schematics/angular/utility/ast-utils';
import { Change, InsertChange } from '@schematics/angular/utility/change';

import { createSourceFile, Node, ScriptTarget, SourceFile, SyntaxKind } from 'typescript';
import { normalize } from '@angular-devkit/core';


export const ReadIntoSourceFile = (host: Tree, modulePath: string): SourceFile => {
	const text = host.read(modulePath);
	if (text === null) {
		throw new SchematicsException(`File ${modulePath} does not exist.`);
	}
	const sourceText = text.toString('utf-8');

	return createSourceFile(modulePath, sourceText, ScriptTarget.Latest, true);
};

const findSpecificNode = (node: Node, kind: SyntaxKind, search: string): Node | null => {
	if (!node) return null;

	if (node.kind === kind && node.getText() === search) {
		return node;
	}

	for (let child of node.getChildren()) {
		const result = findSpecificNode(child, kind, search);
		if (result !== null) {
			return result;
		}
	}

	return null;
};

const findChildNodeByKind = (node: Node, kind: SyntaxKind): Node | undefined => {
	for (let child of node.getChildren()) {
		if (child.kind === kind) return child;
	}
};

const findArrayNodeByName = (node: Node, nodeName: string): Node | undefined => {
	if (!node) return;

	if (node.kind === SyntaxKind.VariableDeclaration) {
		const foundNode = node.forEachChild((child) => {
			if (child.kind === SyntaxKind.Identifier && child.getText() === nodeName) {
				return child;
			}
		});

		if (foundNode) {
			return node.forEachChild((child) => {
				if (child.kind === SyntaxKind.ArrayLiteralExpression) {
					return child;
				}
			});
		}
	}

	for (let child of node.getChildren()) {
		const result = findArrayNodeByName(child, nodeName);
		if (result) return result;
	}
};

const getArraySyntaxList = (node: Node): Node | undefined => {
	for (let child of node.getChildren()) {
		if (child.kind === SyntaxKind.SyntaxList) {
			return child;
		}
	}
};

const _AddToModuleArray = (source: SourceFile, module: string, searchSymbol: string, newSymbol: string): Change[] => {
	const node = findArrayNodeByName(source, searchSymbol);

	if (!node) {
		return [];
	}

	const syntaxList = getArraySyntaxList(node);

	if (!syntaxList) {
		throw new SchematicsException('Something went wrong getting the SyntaxList from the ArrayLiteralExpression');
	}

	let toInsert = '';
	let position = 0;
	if (syntaxList.getChildCount() === 0) { // Empty List
		position = syntaxList.pos++;
		toInsert = `\n\t${newSymbol}`;
	} else { // Already has Items
		let duplicate;
		for (let iNode of syntaxList.getChildren()) {
			if (iNode.kind === SyntaxKind.Identifier && iNode.getText() === newSymbol) {
				duplicate = iNode;
			}
		}

		if (!duplicate) {
			position = syntaxList.getEnd();
			toInsert = `,\n\t${newSymbol}`;
		}
	}

	if (!toInsert || toInsert == '') {
		return [];
	}

	return [new InsertChange(module, position, toInsert)];
};

export const AddToModuleArray = (options: AddDeclarationToModuleOptions) =>
	(host: Tree, context: SchematicContext) => {
		const module = `/${normalize(options.modulePath)}/${options.moduleName}/${options.moduleName}.Module.ts`;
		let moduleSource = ReadIntoSourceFile(host, module);

		const changes = _AddToModuleArray(moduleSource, module, `${options.itemType}s`, `${options.itemName}${options.itemType}`);
		const importFile = options.itemType === 'Component' ? `./${options.itemType}s/${options.itemName}` : `./${options.itemType}s`;
		changes.push(insertImport(moduleSource, module, `${options.itemName}${options.itemType}`, `./${options.itemType}s`));

		const changeRecorder = host.beginUpdate(module);
		for (let change of changes) {
			if (change instanceof InsertChange) {
				changeRecorder.insertLeft(change.pos, change.toAdd);
			}
		}
		host.commitUpdate(changeRecorder);

		return host;
	};

export interface AddDeclarationToModuleOptions {
	modulePath: string;
	moduleName: string;
	itemType: 'Component' | 'Directive' | 'Pipe' | 'Service';
	itemName: string;
}

function showTree(node: Node, indent: string = '  '): void {

  console.log(`${indent}${SyntaxKind[node.kind]} (${node.kind})`);

  if (node.parent && node.parent.kind === SyntaxKind.VariableDeclaration && node.kind === SyntaxKind.Identifier && node.getText() === 'Components') {
    console.log(`${indent}*`);
  }

  if (node.getChildCount() === 0) {
    console.log(indent + '  Text: ' + node.getText());
  }

  for(let child of node.getChildren()) {
    showTree(child, indent + '  ');
  }
}
